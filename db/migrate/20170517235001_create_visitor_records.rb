class CreateVisitorRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :visitor_records do |t|
      t.references :domain_name, foreign_key: true
      t.date :date
      t.integer :hits

      t.timestamps
    end
  end
end
