# copyright SurvivalTravel and Megasap Sdn Bhd. All Rights Reserved.
GrapeSwaggerRails.options.url = "/swagger_doc"
GrapeSwaggerRails.options.app_name = "Top 5 Website"
GrapeSwaggerRails.options.app_url = "http://localhost:3000"

if Rails.env.production?
	GrapeSwaggerRails.options.app_url  = 'http://rails-tmrnd-test-env.a6m2fwvdcv.ap-southeast-1.elasticbeanstalk.com'
else
	GrapeSwaggerRails.options.app_url  = 'http://localhost:3000'
end