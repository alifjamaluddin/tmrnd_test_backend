require "grape-swagger"

module API
  class Base < Grape::API
    format :json

    mount API::V1::Base

    add_swagger_documentation(
      hide_documentation_path: true,
      hide_format: true,
      api_version: "v1",
      info: {
      	title: "API for Top Web Data",
      	description: "These is the list of required API for TM R&D Test",
      	contact_name: "Alif Jamaluddin",
      	contact_email: "muhamad.alif.jamaluddin@gmail.com"
      }
    )
  end
end

