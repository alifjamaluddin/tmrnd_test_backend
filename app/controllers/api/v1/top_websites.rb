module API
  module V1
    class TopWebsites  < Grape::API

      resource :data, desc: "API for top website data" do

        ##### Upload data
        desc "Upload data"
        params do
          requires :file , :type => Rack::Multipart::UploadedFile, :desc => "CSV file"
        end
        post 'upload' do
          require 'csv'   
          file = params[:file]

          csv_text = File.read(file.tempfile)
          csv = CSV.parse(csv_text, {:col_sep => "|"})
          counter = 0
          csv.each_with_index do |row,i|
            next if i == 0
            puts row[1]
            domain_name = DomainName.find_or_create_by(name: row[1])
            domain_hits = VisitorRecord.find_or_create_by(domain_name_id: domain_name.id, date: row[0], hits: row[2])
          end
          status 200
          # return file
        end


        desc "Get top 5 web"
        get 'top_5' do 
          record = VisitorRecord.group(:domain_name).order('sum_hits DESC').limit(5).sum(:hits)
          data = []
          record.each do |a|
            data.push({
              id: a.first.id,
              name: a.first.name,
              hits: a.last
            })
          end

          present data, with: Presenters::TopWeb
        end

        desc "Top web by date"

        params do 
          requires :start_date , :type => Date, :desc => "Start Date i.e 2013-01-06"
          requires :end_date , :type => Date, :desc => "End Date i.e 2013-01-13"
          requires :web_no , :type => Integer, :desc => "Number of web"
        end
        get 'top_web_by_date' do 
          record = VisitorRecord.where("date >= ? AND date <= ?", params[:start_date], params[:end_date]).group(:domain_name).order('sum_hits DESC').limit(params[:web_no]).sum(:hits)

          data = []

         
            record.each do |a|
              data.push({
                id: a[0].id,
                name: a[0].name,
                hits: a[1]
              })
            end
            present data, with: Presenters::TopWeb

        end



        desc "Web visitor hits record"

        params do 
          requires :domain_name_id , :type => Integer, :desc => "Domain name id"
        end
        get 'web_visitor_hits' do 
          record = VisitorRecord.where(domain_name_id: params[:domain_name_id].to_i)

         
          present record, with: Presenters::WebHits
          

        end





      end
    end
  end
end
