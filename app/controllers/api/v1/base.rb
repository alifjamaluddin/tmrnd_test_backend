module API
  module V1
    class Base < Grape::API
      version 'v1'

      helpers do
        def permitted_params
         declared(params, { include_missing: false })
        end

        def current_user
          @current_user
        end
      end

      before do
        Rails.logger.info "Request params: #{params.to_h}"
      end

      mount API::V1::TopWebsites
    end
  end
end
