module API
	module Presenters
		class TopWeb < Grape::Entity
			expose :id,  documentation: {type: "integer", desc: "id", required: true}
			expose :name,  documentation: {type: "integer", desc: "name", required: true}
			expose :hits,  documentation: {type: "string", desc: "Hits number", required: true}
		end
	end
end
