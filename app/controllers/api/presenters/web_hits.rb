module API
	module Presenters
		class WebHits < Grape::Entity
			expose :date,  documentation: {type: "integer", desc: "Date", required: true}
			expose :hits,  documentation: {type: "integer", desc: "Hits ", required: true}
		end
	end
end
